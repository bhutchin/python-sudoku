from datetime import datetime

ROW_COUNT = 9
COLUMN_COUNT = 9

ROW_NUMBERS = [1, 2, 3, 4, 5, 6, 7, 8, 9]

BOARD = [
    [8, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 3, 6, 0, 0, 0, 0, 0],
    [0, 7, 0, 0, 9, 0, 2, 0, 0],
    [0, 5, 0, 0, 0, 7, 0, 0, 0],
    [0, 0, 0, 0, 4, 5, 7, 0, 0],
    [0, 0, 0, 1, 0, 0, 0, 3, 0],
    [0, 0, 1, 0, 0, 0, 0, 6, 8],
    [0, 0, 8, 5, 0, 0, 0, 1, 0],
    [0, 9, 0, 0, 0, 0, 4, 0, 0]
]


##Small squres operate in below order
## 1 | 4 | 7
## 2 | 5 | 8
## 3 | 6 | 9


def check_column(board):
    for i in range(0, 8):
        column = []
        for row in board:
            column.append(row[i])
        for j in range(1, 10):
            if column.count(j) > 1:
                #print('dupe number in column ' + str(i + 1))
                return False
    return True


def get_numbers_used_in_row(row_number, board):
    row = board[row_number-1]
    return list(set(row))


def get_numbers_used_in_column(column_number, board):
    column_number = column_number - 1
    column = []
    for row in board:
        column.append(row[column_number])
    return list(set(column))


def check_row(board):
    for row in enumerate(board, start=1):
        for i in range(1, 9):
            if row[1].count(i) > 1:
                #print('dupe number in row ' + str(row[0]))
                return False
    return True


def check_small_square(board):
    square_count = 1
    start_count = 0
    square = []
    while start_count <= 6:
        for row_number, row in enumerate(board, start=1):

            square.extend(row[start_count:start_count+3])
            if row_number % 3 == 0:
                for j in range(1, 10):
                    if square.count(j) > 1:
                        print('Dupe in square ' + str(square_count))
                        return False
                square = []
                square_count = square_count + 1
        start_count = start_count + 3
    return True


def find_empty_space(board):
    for row_number, row in enumerate(board, start=1):
        for column_number, digit in enumerate(row, start=1):
            if digit == 0:
                empty_space = (row_number, column_number)
                return empty_space
    return 0


def check_board(board):
    if not check_row(board):
        return False
    if not check_column(board):
        return False
    if not check_small_square(board):
        return False
    return True


def get_valid_numbers(space, board):
    used_row_numbers = get_numbers_used_in_row(space[0], board)
    used_column_numbers = get_numbers_used_in_column(space[1], board)
    valid_numbers = list(set(ROW_NUMBERS).difference(set(used_row_numbers)))
    valid_numbers = list(set(valid_numbers).difference(set(used_column_numbers)))
    return valid_numbers


def make_move(board):
    empty_space = find_empty_space(board)
    if empty_space == 0:
        return board
    valid_numbers = get_valid_numbers(empty_space, board)
    if valid_numbers:
        for i in valid_numbers:
            board[empty_space[0]-1][empty_space[1]-1] = i
            if check_board(board):
                if make_move(board):
                    return board
            print('Backtracing')
            board[empty_space[0]-1][empty_space[1]-1] = 0


if __name__ == "__main__":
    START_TIME = datetime.now()
    SOLVED_BOARD = make_move(BOARD)
    for ROW in SOLVED_BOARD:
        print(ROW)
    print('Completed in: ' + str(datetime.now() - START_TIME))